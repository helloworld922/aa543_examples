find_package(Doxygen)
option(BUILD_DOCUMENTATION "Create and install the HTML based documentation (requires Doxygen)" ${DOXYGEN_FOUND})
if(BUILD_DOCUMENTATION)
  if(NOT DOXYGEN_FOUND)
    message(FATAL_ERROR "Doxygen is needed to build the documentation.")
  endif()

  set(doxyfile "${CMAKE_CURRENT_BINARY_DIR}/doxyfile")
  configure_file("${CMAKE_CURRENT_SOURCE_DIR}/doxyfile.in"
    ${doxyfile} @ONLY)
  add_custom_target(AA543_EXAMPLES_DOC
    COMMAND ${DOXYGEN_EXECUTABLE} ${doxyfile}
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    COMMENT "Generating documentation with Doxygen"
    VERBATIM)
  install(DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/html"
    DESTINATION "${DOC_INSTALL_DIR}/${PROJECT_NAME}"
    COMPONENT docs
    PATTERN "html/*"
    )
  install(FILES "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE"
    DESTINATION "${DOC_INSTALL_DIR}/${PROJECT_NAME}"
    )
endif()