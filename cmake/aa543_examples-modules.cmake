# TODO: Here is where user space modules can be loaded

find_package(OpenMP)
if(OPENMP_FOUND)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
endif()

#find_package(OpenCL)
include("FindOpenCL")
if(OpenCL_FOUND)
  include_directories(${OPENCL_INCLUDE_DIRS})
  link_directories(${OpenCL_LIBRARY_DIRS})
endif()

