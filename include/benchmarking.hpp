#ifndef BENCHMARKING_HPP
#define BENCHMARKING_HPP

#include <chrono>
#include <vector>
#include <cmath>
#include <iostream>

template<class FUNCTOR>
std::chrono::nanoseconds benchmark(size_t min_iters, size_t max_iters, std::chrono::nanoseconds stop_time, FUNCTOR& f)
{
  using namespace std::chrono;
  high_resolution_clock timer;
  nanoseconds min_t = nanoseconds::max();
  std::vector<nanoseconds> times;
  nanoseconds sum = nanoseconds::zero();
  f.init();
  f.run();
  f.cleanup();
  for(size_t i = 0; i < max_iters; ++i)
  {
    f.init();
    auto start = timer.now();
    f.run();
    auto stop = timer.now();
    f.cleanup();
    auto t = duration_cast<nanoseconds>(stop - start);
    if(t < min_t)
    {
      min_t = t;
    }
    sum += t;
    if(i > min_iters && sum >= stop_time)
    {
      return sum/i;
    }
  }
  return sum/max_iters;
}
#endif
