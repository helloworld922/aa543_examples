#ifndef FILE_TOOLS_HPP
#define FILE_TOOLS_HPP

#include <string>

/// opens file relative to executable
std::string get_file_contents(const char* filename);

#endif
