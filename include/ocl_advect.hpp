#ifndef OCL_ADVECT_HPP
#define OCL_ADVECT_HPP

#include "ocl_use_hptr.hpp"
#include "ocl_alloc_hptr.hpp"
#include "ocl_plain_buf.hpp"
#include "ocl_advect1.hpp"
#include "ocl_advect2.hpp"
#include "ocl_advect3.hpp"

#endif
