#ifndef OCL_ADVECT1_HPP
#define OCL_ADVECT1_HPP

#include "ocl_advect_base.hpp"

struct ocl_advect1 : public ocl_advect_base
{
  ocl_advect1(size_t num_elems, ocl_base& b, real a = 1, size_t num_steps = 1000);

  void init() override;

  static std::string kernel_name()
  {
    return "ocl_advect1";
  }

  static std::string prog_source();
};

#endif
