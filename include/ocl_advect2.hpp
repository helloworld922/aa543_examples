#ifndef OCL_ADVECT2_HPP
#define OCL_ADVECT2_HPP

#include "ocl_advect_base.hpp"

struct ocl_advect2 : public ocl_advect_base
{
  ocl_advect2(size_t num_elems, ocl_base& b, real a = 1, size_t num_steps = 1000);

  static std::string kernel_name()
  {
    return "ocl_advect2";
  }

  static std::string prog_source();

  void init() override;
};

#endif
