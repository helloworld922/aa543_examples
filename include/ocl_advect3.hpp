#ifndef OCL_ADVECT3_HPP
#define OCL_ADVECT3_HPP

#include "ocl_advect_base.hpp"

struct ocl_advect3 : public ocl_advect_base
{
  size_t buf_mult;
  ocl_advect3(size_t num_elems, ocl_base& b, real a = 1, size_t num_steps = 1000, size_t buf_mult=1);

  void init() override;

  static std::string kernel_name()
  {
    return "ocl_advect3";
  }

  static std::string prog_source();

  void run() override;
};

#endif
