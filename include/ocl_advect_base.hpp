#ifndef OCL_ADVECT_BASE_HPP
#define OCL_ADVECT_BASE_HPP

#include "ocl_base.hpp"

struct ocl_advect_base
{
  real a;
  size_t num_elems;
  real dx;
  real dt;
  size_t num_steps;

  ocl_base &ocl_;

  ocl_advect_base(size_t num_elems, ocl_base& b, real a = 1,
		  size_t num_steps = 1000);

  virtual ~ocl_advect_base();

  virtual void init() = 0;
  virtual void run();
  virtual void cleanup();
};

#endif
