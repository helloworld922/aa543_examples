#ifndef OCL_ALLOC_HPTR_HPP
#define OCL_ALLOC_HPTR_HPP

#include "ocl_base.hpp"

struct ocl_alloc_hptr : public ocl_base
{
  ocl_alloc_hptr(const std::string& source, const char* kernel_name);

  void create_mem_bufs(size_t n) override;

  ~ocl_alloc_hptr();
};

#endif
