#ifndef OCL_BASE_HPP
#define OCL_BASE_HPP

#include <CL/cl.h>
#include <string>
#include <cstddef>


typedef double real;

struct ocl_base
{
  cl_context context;
  cl_command_queue queue;

  cl_program program;
  cl_kernel kernel;

  cl_mem qold_buf;
  cl_mem qnew_buf;

  size_t n_mult;

  bool have_bufs;
  
  ocl_base(const std::string& source, const char* kernel_name);

  virtual void create_mem_bufs(size_t n) = 0;

  virtual void free_mem_bufs();

  virtual ~ocl_base();
};

#endif
