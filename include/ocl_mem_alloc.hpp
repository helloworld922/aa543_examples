#ifndef OCL_MEM_ALLOC_HPP
#define OCL_MEM_ALLOC_HPP

#include <CL/cl.h>

struct ocl_mem_alloc_base
{
  cl_mem qold_buf;
  cl_mem qnew_buf;

};

#endif
