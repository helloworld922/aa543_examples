#ifndef OCL_PLAIN_BUF_HPP
#define OCL_PLAIN_BUF_HPP

#include "ocl_base.hpp"

struct ocl_plain_buf : public ocl_base
{
  real* qnew;
  real* qold;
  
  ocl_plain_buf(const std::string& source, const char* kernel_name);

  void create_mem_bufs(size_t n) override;

  void free_mem_bufs() override;

  ~ocl_plain_buf();
};

#endif
