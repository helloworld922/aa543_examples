#ifndef OCL_USE_HPTR_HPP
#define OCL_USE_HPTR_HPP

#include "ocl_base.hpp"

struct ocl_use_hptr : public ocl_base
{
  real* qnew;
  real* qold;
  
  ocl_use_hptr(const std::string& source, const char* kernel_name);

  void create_mem_bufs(size_t n) override;

  void free_mem_bufs() override;

  ~ocl_use_hptr();
};

#endif
