#ifndef OMP_ADVECT_HPP
#define OMP_ADVECT_HPP

#include <vector>
#include <cstddef>
#include <omp.h>

struct omp_advect_base
{
  size_t num_threads;
  std::vector<double> qold;
  std::vector<double> qnew;
  double a;
  size_t num_elems;
  double dx;
  double dt;
  size_t num_steps;

  // domain: [-10,10]
  omp_advect_base(size_t num_threads,
		  size_t num_elems,
		  double a = 1,
		  size_t num_steps = 1000) : num_threads(num_threads), a(a), num_elems(num_elems), dx(20./num_elems),
					     dt(20./num_elems), num_steps(num_steps)
  {
    omp_set_dynamic(0);
    omp_set_num_threads(num_threads);
  }

  virtual void init() = 0;
  virtual void run() = 0;
  virtual void cleanup()
  {}
};

/// all u's, then all v's
/// fixed bc=0
struct omp_advect1 : public omp_advect_base
{
  omp_advect1(size_t num_threads,
	      size_t num_elems,
	      double a = 1,
	      size_t num_steps = 1000) : omp_advect_base(num_threads, num_elems, a, num_steps)
  {}
  
  void init() override;
  void run() override;
};

/// interleaved u,v
/// fixed bc=0
struct omp_advect2 : public omp_advect_base
{
  omp_advect2(size_t num_threads,
	      size_t num_elems,
	      double a = 1,
	      size_t num_steps = 1000) : omp_advect_base(num_threads, num_elems, a, num_steps)
  {}

  void init() override;
  void run() override;
};
#endif
