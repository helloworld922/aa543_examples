#ifndef OMP_TILED_HPP
#define OMP_TILED_HPP

#include "omp_advect.hpp"

struct omp_tiled : public omp_advect_base
{
  size_t num_tiles;
  size_t tile_size;
  
  omp_tiled(size_t num_threads,
	    size_t num_elems,
	    double a=1,
	    size_t tile_size=64,
	    size_t num_steps = 1000);

  void init() override;
  void run() override;
};

#endif
