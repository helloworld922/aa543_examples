#ifndef VM_MULT_HPP
#define VM_MULT_HPP

#include <vector>
#include <cstddef>

struct vm_mult_base
{
  size_t n;
  std::vector<double> mat;
  std::vector<double> vec;
  std::vector<double> res;

  vm_mult_base(size_t n) : n(n)
  {}
  
  virtual void init();

  virtual void run() = 0;

  virtual void cleanup()
  {
  }
};

struct vm_mult1 : public vm_mult_base
{
  vm_mult1(size_t n) : vm_mult_base(n)
  {}
  
  void run() override;
};

struct vm_mult2 : public vm_mult_base
{
  vm_mult2(size_t n) : vm_mult_base(n)
  {}
  
  void run() override;
};

#endif
