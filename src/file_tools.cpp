#include "file_tools.hpp"
#include <fstream>
#include <unistd.h>
#include <vector>

#include <iostream>

std::string get_file_contents(const char *filename)
{
  char buf[1024];
  int len = readlink("/proc/self/exe", buf, sizeof(buf)-1);
  for(int i = len-1; i >= 0; --i)
  {
    if(buf[i] == '/')
    {
      buf[i] = '\0';
      break;
    }
  }
  std::string f(buf);
  f += "/kernels/";
  f += filename;
  std::ifstream in(f, std::ios::in | std::ios::binary);
  std::string contents;
  in.seekg(0, std::ios::end);
  contents.resize(in.tellg());
  in.seekg(0, std::ios::beg);
  in.read(&contents[0], contents.size());
  return contents;
}
