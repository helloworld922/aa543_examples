kernel void ocl_advect1(global const real* qold, global real* qnew,
			const real dx, const real dt, const real a)
{
  const real cfl = a*dt/dx;
  const size_t i = get_global_id(0) + 1;
  const size_t n = get_global_size(0)+2;
  const real un = qold[i-1];
  const real u = qold[i];
  const real up = qold[i+1];
  const real vn = qold[i-1+n];
  const real v = qold[i+n];
  const real vp = qold[i+1+n];
  qnew[i] = u-cfl*(u-(up+un-vp+vn)*.5);
  qnew[i+n] = v-cfl*(v-(vp+vn-up+un)*.5);
}
