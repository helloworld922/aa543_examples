kernel void ocl_advect2(global const real* qold, global real* qnew,
			const real dx, const real dt, const real a)
{
  const real cfl = a*dt/dx;
  const size_t i = get_global_id(0) + 1;
  const real un = qold[2*(i-1)];
  const real vn = qold[2*(i-1)+1];
  const real u = qold[2*i];
  const real v = qold[2*i+1];
  const real up = qold[2*(i+1)];
  const real vp = qold[2*(i+1)+1];
  qnew[2*i] = u-cfl*(u-(up+un-vp+vn)*.5);
  qnew[2*i+1] = v-cfl*(v-(vp+vn-up+un)*.5);
}
