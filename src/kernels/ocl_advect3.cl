// buf overlap: 1 on left, 2 on right
// responsible for right boundary
// buf is 2*bsize (for storing u and v)
kernel void ocl_advect3(global const real* qold, global real* qnew,
			const real dx, const real dt, const real a,
			local real* buf, const uint bsize)
{
  const size_t lid = get_local_id(0);
  const size_t gid = get_group_id(0);
  const size_t lsize = get_local_size(0);
  const size_t n = get_num_groups(0)*(bsize-2)+2;
  const real cfl = a*dt/dx;

  // cache
#ifndef MANUAL_CACHING
  {
    event_t events[2];
    events[0] = async_work_group_copy(buf, qold+(bsize-2)*gid, bsize, 0);
    events[1] = async_work_group_copy(buf+bsize, qold+(bsize-2)*gid+n, bsize, 0);
    wait_group_events(2,events);
  }
#else
  // cache u
  for(size_t j = lid; j < bsize; j += lsize)
  {
    buf[j] = qold[j+(bsize-2)*gid];
  }
  // cache v
  for(size_t j = lid; j < bsize; j += lsize)
  {
    buf[j+bsize] = qold[j+n+(bsize-2)*gid];
  }
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

  for(size_t j = lid; j < bsize; j += lsize)
  {
    if(j > 0 && j < bsize-1)
    {
      // can do work
      const real un = buf[j-1];
      const real u = buf[j];
      const real up = buf[j+1];
      const real vn = buf[j-1+bsize];
      const real v = buf[j+bsize];
      const real vp = buf[j+1+bsize];
      const size_t i = j+(bsize-2)*gid;
      qnew[i] = u-cfl*(u-(up+un-vp+vn)*.5);
      qnew[i+n] = v-cfl*(v-(vp+vn-up+un)*.5);
    }
  }
}
