#include "ocl_advect1.hpp"
#include "file_tools.hpp"
#include <cmath>
#include <vector>

ocl_advect1::ocl_advect1(size_t num_elems, ocl_base& b, real a, size_t num_steps)
  : ocl_advect_base(num_elems, b, a, num_steps)
{
}

std::string ocl_advect1::prog_source()
{
  return get_file_contents("ocl_advect1.cl");
}

void ocl_advect1::init()
{
  // initial conditions
  cl_int err;
  const size_t buf_size = sizeof(real)*2*num_elems;

#ifdef USE_MAP
  auto qold = (real*) clEnqueueMapBuffer(ocl_.queue,
					 ocl_.qold_buf,
					 CL_FALSE,
					 CL_MAP_WRITE_INVALIDATE_REGION,
					 0,
					 buf_size,
					 0,
					 nullptr,
					 nullptr,
					 &err);
  auto qnew = (real*) clEnqueueMapBuffer(ocl_.queue,
					 ocl_.qnew_buf,
					 CL_TRUE,
					 CL_MAP_WRITE_INVALIDATE_REGION,
					 0,
					 buf_size,
					 0,
					 nullptr,
					 nullptr,
					 &err);
#else
  std::vector<real> qold(2*num_elems);
  std::vector<real> qnew(2*num_elems);
#endif
  for(size_t i = 0; i < 2*num_elems; ++i)
  {
    qold[i] = 0;
    qnew[i] = 0;
  }
  for(size_t i = num_elems*.45; i < num_elems*.55; ++i)
  {
    real x = dx * (i-num_elems/2.);
    qold[i] = sin(2.*3.141592653589793*x);
    qold[i+num_elems] = cos(2.*3.141592653589793*x);
    qnew[i] = qold[i];
    qnew[i+num_elems] = qold[i+num_elems];
  }
#ifdef USE_MAP
  clEnqueueUnmapMemObject(ocl_.queue, ocl_.qold_buf, qold, 0, nullptr, nullptr);
  clEnqueueUnmapMemObject(ocl_.queue, ocl_.qnew_buf, qnew, 0, nullptr, nullptr);
#else
  clEnqueueWriteBuffer(ocl_.queue, ocl_.qold_buf, CL_FALSE, 0, sizeof(real)*qold.size(), qold.data(), 0, nullptr, nullptr);
  clEnqueueWriteBuffer(ocl_.queue, ocl_.qnew_buf, CL_TRUE, 0, sizeof(real)*qnew.size(), qnew.data(), 0, nullptr, nullptr);
#endif
}
