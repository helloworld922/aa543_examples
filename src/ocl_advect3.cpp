#include "ocl_advect3.hpp"
#include "file_tools.hpp"
#include <cmath>
#include <vector>
#include <iostream>

ocl_advect3::ocl_advect3(size_t num_elems, ocl_base& b, real a, size_t num_steps, size_t buf_mult)
  : ocl_advect_base(num_elems, b, a, num_steps), buf_mult(buf_mult)
{
}

std::string ocl_advect3::prog_source()
{
  return get_file_contents("ocl_advect3.cl");
}

void ocl_advect3::init()
{
  // initial conditions
  cl_int err;
  const size_t buf_size = sizeof(real)*2*num_elems;

#ifdef USE_MAP
  auto qold = (real*) clEnqueueMapBuffer(ocl_.queue,
					 ocl_.qold_buf,
					 CL_FALSE,
					 CL_MAP_WRITE_INVALIDATE_REGION,
					 0,
					 buf_size,
					 0,
					 nullptr,
					 nullptr,
					 &err);
  auto qnew = (real*) clEnqueueMapBuffer(ocl_.queue,
					 ocl_.qnew_buf,
					 CL_TRUE,
					 CL_MAP_WRITE_INVALIDATE_REGION,
					 0,
					 buf_size,
					 0,
					 nullptr,
					 nullptr,
					 &err);
#else
  std::vector<real> qold(2*num_elems);
  std::vector<real> qnew(2*num_elems);
#endif
  for(size_t i = 0; i < 2*num_elems; ++i)
  {
    qold[i] = 0;
    qnew[i] = 0;
  }
  for(size_t i = num_elems*.45; i < num_elems*.55; ++i)
  {
    real x = dx * (i-num_elems/2.);
    qold[i] = sin(2.*3.141592653589793*x);
    qold[i+num_elems] = cos(2.*3.141592653589793*x);
    qnew[i] = qold[i];
    qnew[i+num_elems] = qold[i+num_elems];
  }
#ifdef USE_MAP
  clEnqueueUnmapMemObject(ocl_.queue, ocl_.qold_buf, qold, 0, nullptr, nullptr);
  clEnqueueUnmapMemObject(ocl_.queue, ocl_.qnew_buf, qnew, 0, nullptr, nullptr);
#else
  clEnqueueWriteBuffer(ocl_.queue, ocl_.qold_buf, CL_FALSE, 0, sizeof(real)*qold.size(), qold.data(), 0, nullptr, nullptr);
  clEnqueueWriteBuffer(ocl_.queue, ocl_.qnew_buf, CL_TRUE, 0, sizeof(real)*qnew.size(), qnew.data(), 0, nullptr, nullptr);
#endif
}

void ocl_advect3::run()
{
  cl_int err;
  //const size_t global_work_offset[] = {0};
  const unsigned buf_size = ocl_.n_mult*buf_mult;
  const size_t local_work_size[] = {ocl_.n_mult};
  const size_t global_work_size[] = {(num_elems-2)/(buf_size-2) * ocl_.n_mult};
  err = clSetKernelArg(ocl_.kernel, 2, sizeof(real), &dx);
  err = clSetKernelArg(ocl_.kernel, 3, sizeof(real), &dt);
  err = clSetKernelArg(ocl_.kernel, 4, sizeof(real), &a);
  err = clSetKernelArg(ocl_.kernel, 5, sizeof(real)*2*buf_size, nullptr);
  err = clSetKernelArg(ocl_.kernel, 6, sizeof(unsigned), &buf_size);
  for(size_t i = 0; i < num_steps; ++i)
  {
    // TODO: enqueue kernel
    clSetKernelArg(ocl_.kernel, 0, sizeof(cl_mem), &ocl_.qold_buf);
    clSetKernelArg(ocl_.kernel, 1, sizeof(cl_mem), &ocl_.qnew_buf);
    err = clEnqueueNDRangeKernel(ocl_.queue,
				 ocl_.kernel,
				 1,
				 NULL,
				 global_work_size,
				 local_work_size,
				 0,
				 nullptr,
				 nullptr);
    std::swap(ocl_.qold_buf, ocl_.qnew_buf);
  }
  clFinish(ocl_.queue);
}
