#include "ocl_advect_base.hpp"
#include <iostream>


ocl_advect_base::ocl_advect_base(size_t num_elems, ocl_base& b, real a,
				 size_t num_steps)
  : a(a), num_elems(num_elems), dx(20./num_elems),
    dt(20./num_elems), num_steps(num_steps), ocl_(b)
{
}

ocl_advect_base::~ocl_advect_base()
{
}

void ocl_advect_base::run()
{
  cl_int err;
  const size_t global_work_offset[] = {0};
  const size_t global_work_size[] = {num_elems-2};
  const size_t local_work_size[] = {ocl_.n_mult};
  err = clSetKernelArg(ocl_.kernel, 2, sizeof(real), &dx);
  err = clSetKernelArg(ocl_.kernel, 3, sizeof(real), &dt);
  err = clSetKernelArg(ocl_.kernel, 4, sizeof(real), &a);
  for(size_t i = 0; i < num_steps; ++i)
  {
    // TODO: enqueue kernel
    clSetKernelArg(ocl_.kernel, 0, sizeof(cl_mem), &ocl_.qold_buf);
    clSetKernelArg(ocl_.kernel, 1, sizeof(cl_mem), &ocl_.qnew_buf);
    err = clEnqueueNDRangeKernel(ocl_.queue,
				 ocl_.kernel,
				 1,
				 NULL,
				 global_work_size,
				 local_work_size,
				 //nullptr,
				 0,
				 nullptr,
				 nullptr);
    if(err != CL_SUCCESS)
    {
      std::cout << "kernel " << err << std::endl;
    }
    std::swap(ocl_.qold_buf, ocl_.qnew_buf);
  }
  clFinish(ocl_.queue);
}

void ocl_advect_base::cleanup()
{
}
