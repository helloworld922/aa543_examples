//#define USE_MAP

#include "ocl_advect.hpp"
#include "benchmarking.hpp"

#include <iostream>
#include <fstream>
#include <cmath>
#include <CL/cl.h>

typedef ocl_plain_buf alloc_type;
//typedef ocl_use_hptr alloc_type;
//typedef ocl_alloc_hptr alloc_type;

namespace std
{
  std::ostream& operator<<(std::ostream& o, std::vector<std::chrono::nanoseconds>& v)
  {
    o << "[";
    if(v.size())
    {
      o << v[0].count() * 1e-9;
    }
    for(size_t i = 1; i < v.size(); ++i)
    {
      o << ", " << v[i].count() * 1e-9;
    }
    o << "]";
    return o;
  }

  std::ostream& operator<<(std::ostream& o, std::vector<size_t>& v)
  {
    o << "[";
    if(v.size())
    {
      o << v[0];
    }
    for(size_t i = 1; i < v.size(); ++i)
    {
      o << ", " << v[i];
    }
    o << "]";
    return o;
  }

  std::ostream& operator<<(std::ostream& o, std::vector<double>& v)
  {
    o << "[";
    if(v.size())
    {
      o << v[0];
    }
    for(size_t i = 1; i < v.size(); ++i)
    {
      o << ", " << v[i];
    }
    o << "]";
    return o;
  }
}

template<class T>
void print_qold(std::ofstream& o, T& vm)
{
  cl_int err;
  const size_t buf_size = sizeof(real)*2*vm.num_elems;
#ifdef USE_MAP
  auto qold = (real*) clEnqueueMapBuffer(vm.ocl_.queue,
					 vm.ocl_.qold_buf,
					 CL_TRUE,
					 CL_MAP_WRITE_INVALIDATE_REGION,
					 0,
					 buf_size,
					 0,
					 nullptr,
					 nullptr,
					 &err);
#else
  std::vector<real> qold(2*vm.num_elems);
  clEnqueueReadBuffer(vm.ocl_.queue,
		      vm.ocl_.qold_buf,
		      CL_TRUE,
		      0,
		      buf_size,
		      qold.data(),
		      0,
		      nullptr,
		      nullptr);
#endif
  o << "[";
  if(vm.num_elems)
  {
    o << qold[0];
  }
  for(size_t i = 1; i < 2*vm.num_elems; ++i)
  {
    o << ", " << qold[i];
  }
  o << "]";
#ifdef USE_MAP
  clEnqueueUnmapMemObject(vm.ocl_.queue,
			  vm.ocl_.qold_buf,
			  qold,
			  0,
			  nullptr,
			  nullptr);
#endif
}


template<class T>
void print_qnew(std::ofstream& o, T& vm)
{
  cl_int err;
  const size_t buf_size = sizeof(real)*2*vm.num_elems;

#ifdef USE_MAP
  auto qnew = (real*) clEnqueueMapBuffer(vm.ocl_.queue,
					 vm.ocl_.qnew_buf,
					 CL_TRUE,
					 CL_MAP_WRITE_INVALIDATE_REGION,
					 0,
					 buf_size,
					 0,
					 nullptr,
					 nullptr,
					 &err);
#else
  std::vector<real> qnew(2*vm.num_elems);
  clEnqueueReadBuffer(vm.ocl_.queue,
		      vm.ocl_.qnew_buf,
		      CL_TRUE,
		      0,
		      buf_size,
		      qnew.data(),
		      0,
		      nullptr,
		      nullptr);
#endif

  o << "[";
  if(vm.num_elems)
  {
    o << qnew[0];
  }
  for(size_t i = 1; i < 2*vm.num_elems; ++i)
  {
    o << ", " << qnew[i];
  }
  o << "]";
  clEnqueueUnmapMemObject(vm.queue, vm.qnew_buf, qnew, 0, nullptr, nullptr);
}

void test_output(size_t beta)
{
  size_t num_steps = 10;
#if 1
  {
    std::ofstream out("ocladvect1.out");
    alloc_type base(ocl_advect1::prog_source(), ocl_advect1::kernel_name().c_str());
    std::cout << "got ocl_base" << std::endl;
    ocl_advect1 vm(beta*base.n_mult+2, base, 1, 1);
    std::cout << "got ocl_advect1" << std::endl;
    base.create_mem_bufs(vm.num_elems*2);
    std::cout << "created mem bufs" << std::endl;
    vm.init();
    std::cout << "init" << std::endl;
    for(size_t i = 0; i < num_steps; ++i)
    {
      print_qold(out, vm);
      out << std::endl;
      vm.run();
    }
  }
#endif
#if 1
  {
    std::ofstream out("ocladvect2.out");
    alloc_type base(ocl_advect2::prog_source(), ocl_advect2::kernel_name().c_str());
    std::cout << "got ocl_base" << std::endl;
    ocl_advect2 vm(beta*base.n_mult+2, base, 1, 1);
    std::cout << "got ocl_advect2" << std::endl;
    base.create_mem_bufs(vm.num_elems*2);
    std::cout << "created mem bufs" << std::endl;
    vm.init();
    std::cout << "init" << std::endl;
    for(size_t i = 0; i < num_steps; ++i)
    {
      print_qold(out, vm);
      out << std::endl;
      vm.run();
    }
  }
#endif
#if 1
  {
    std::ofstream out("ocladvect3.out");
    alloc_type base(ocl_advect3::prog_source(), ocl_advect3::kernel_name().c_str());
    std::cout << "got ocl_base" << std::endl;
    size_t buf_mult = 1;
    auto loc_beta = beta/buf_mult;
    if(!loc_beta)
    {
      loc_beta = 1;
    }
    if(base.n_mult*buf_mult <= 2)
    {
      buf_mult = 64;
    }
    
    ocl_advect3 vm((base.n_mult*buf_mult-2)*loc_beta+2, base, 1, 1, buf_mult);
    std::cout << "got ocl_advect3 " << vm.num_elems << std::endl;
    base.create_mem_bufs(vm.num_elems*2);
    std::cout << "created mem bufs" << std::endl;
    vm.init();
    std::cout << "init" << std::endl;
    for(size_t i = 0; i < num_steps; ++i)
    {
      print_qold(out, vm);
      out << std::endl;
      vm.run();
    }
  }
#endif
}

size_t roundup(size_t n, size_t mult)
{
  auto rem = (n-2) % mult;
  auto div = (n-2) / mult;
  if(!rem)
  {
    return n;
  }
  return div*mult+2;
}

void do_benchmarking()
{
  using namespace std::chrono;
  constexpr size_t min_iters = 4;
  constexpr size_t max_iters = 1000;
  constexpr size_t num_steps = 1000;
  milliseconds stop_time(250);
  size_t min_mult = 10;
  size_t max_mult = 100001;

#if 0
  {
    alloc_type base(ocl_advect1::prog_source(), ocl_advect1::kernel_name().c_str());
    std::vector<size_t> ns;
    std::vector<nanoseconds> times;
    for(size_t m = min_mult; m < max_mult; m *= 2)
    {
      size_t n = base.n_mult*m+2;
      ocl_advect1 vm(n, base, 1, num_steps);
      base.create_mem_bufs(vm.num_elems*2);
      ns.push_back(n);
      times.push_back(benchmark(min_iters, max_iters, stop_time, vm));
      base.free_mem_bufs();
    }
    std::cout << "ns.append(" << ns << ")" << std::endl;
    std::cout << "vm.append(" << times << ")" << std::endl;
  }
#endif
#if 0
  {
    alloc_type base(ocl_advect2::prog_source(), ocl_advect2::kernel_name().c_str());
    std::vector<size_t> ns;
    std::vector<nanoseconds> times;
    for(size_t m = min_mult; m < max_mult; m *= 2)
    {
      size_t n = base.n_mult*m+2;
      ocl_advect2 vm(n, base, 1, num_steps);
      base.create_mem_bufs(vm.num_elems*2);
      ns.push_back(n);
      times.push_back(benchmark(min_iters, max_iters, stop_time, vm));
      base.free_mem_bufs();
    }
    std::cout << "ns.append(" << ns << ")" << std::endl;
    std::cout << "vm.append(" << times << ")" << std::endl;
  }
#endif
#if 1
  {
    alloc_type base(ocl_advect3::prog_source(), ocl_advect3::kernel_name().c_str());
    for(size_t buf_mult = 1; buf_mult <= 4; ++buf_mult)
    {
      std::vector<size_t> ns;
      std::vector<nanoseconds> times;

      auto loc_min = min_mult/buf_mult;
      auto loc_max = max_mult/buf_mult;
      if(!loc_min)
      {
	loc_min = 0;
      }
      if(loc_max <= loc_min)
      {
	loc_max = loc_min + 1;
      }
      for(size_t m = loc_min; m < loc_max; m *= 2)
      {
	size_t n = (base.n_mult*buf_mult-2)*m+2;
	ocl_advect3 vm(n, base, 1, num_steps, buf_mult);
	base.create_mem_bufs(vm.num_elems*2);
	ns.push_back(vm.num_elems);
	times.push_back(benchmark(min_iters, max_iters, stop_time, vm));
	base.free_mem_bufs();
      }
      std::cout << "ns.append(" << ns << ")" << std::endl;
      std::cout << "vm.append(" << times << ")" << std::endl;
    }
  }
#endif
}

int main()
{
  test_output(100);
  //do_benchmarking();
  return 0;
}
