#include "ocl_alloc_hptr.hpp"

ocl_alloc_hptr::ocl_alloc_hptr(const std::string& source, const char* kernel_name)
  : ocl_base(source, kernel_name)
{
}

void ocl_alloc_hptr::create_mem_bufs(size_t n)
{
  if(have_bufs)
  {
    free_mem_bufs();
  }
  have_bufs = true;
  cl_int err;
  qold_buf = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, sizeof(real)*n,
			    nullptr, &err);
  qnew_buf = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, sizeof(real)*n,
			    nullptr, &err);  
}

ocl_alloc_hptr::~ocl_alloc_hptr()
{
}
