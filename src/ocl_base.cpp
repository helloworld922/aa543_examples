#include "ocl_base.hpp"
#include <iostream>
#include <vector>

template<class T>
struct real_name;

template<>
struct real_name<double>
{
  static constexpr const char* name="double";
};
template<>
struct real_name<float>
{
  static constexpr const char* name="float";
};

ocl_base::ocl_base(const std::string& source, const char* kernel_name) : have_bufs(false)
{
  cl_int err;
  // get platform
  cl_platform_id platform;
  clGetPlatformIDs(1, &platform, nullptr);
  // get a GPU device
  cl_device_id device;
  clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &device, nullptr);
  // create context
  context = clCreateContext(nullptr, 1, &device, nullptr, nullptr, nullptr);
  // create command queue
  queue = clCreateCommandQueue(context, device, 0, nullptr);

  // compile program
  auto src_dat = source.data();
  auto src_size = source.size();
  program = clCreateProgramWithSource(context, 1, &src_dat, &src_size, &err);
  std::string ocl_flags = "-D real=";
  ocl_flags += real_name<real>::name;
  err = clBuildProgram(program, 1, &device, ocl_flags.c_str(), nullptr, nullptr);
  if(err != CL_SUCCESS)
  {
    // print build log
    std::vector<char> buf(100000);
    size_t actual_size = 0;
    clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, buf.size(), buf.data(), &actual_size);
    std::string log(buf.begin(), buf.begin()+actual_size);
    std::cout << log << std::endl;
  }
  // create kernel
  kernel = clCreateKernel(program, kernel_name, &err);
  
  clGetKernelWorkGroupInfo(kernel, device, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, sizeof(size_t), &n_mult, nullptr);
}

void ocl_base::free_mem_bufs()
{
  if(have_bufs)
  {
    clReleaseMemObject(qold_buf);
    clReleaseMemObject(qnew_buf);
    have_bufs = false;
  }
}

ocl_base::~ocl_base()
{
  // cleanup opencl stuff
  free_mem_bufs();
  clReleaseProgram(program);
  clReleaseKernel(kernel);
  clReleaseCommandQueue(queue);
  clReleaseContext(context);
}
