#include "ocl_plain_buf.hpp"

ocl_plain_buf::ocl_plain_buf(const std::string& source, const char* kernel_name)
  : ocl_base(source, kernel_name), qold(nullptr), qnew(nullptr)
{
}

void ocl_plain_buf::create_mem_bufs(size_t n)
{
  if(have_bufs)
  {
    free_mem_bufs();
  }
  have_bufs = true;
  qold = new real[n];
  qnew = new real[n];
  cl_int err;
  qold_buf = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(real)*n,
			    nullptr, &err);
  qnew_buf = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(real)*n,
			    nullptr, &err);
}

void ocl_plain_buf::free_mem_bufs()
{
  ocl_base::free_mem_bufs();
  delete[] qold;
  delete[] qnew;
  qold = qnew = nullptr;
}

ocl_plain_buf::~ocl_plain_buf()
{
  free_mem_bufs();
}
