#include "ocl_use_hptr.hpp"

ocl_use_hptr::ocl_use_hptr(const std::string& source, const char* kernel_name)
  : ocl_base(source, kernel_name), qold(nullptr), qnew(nullptr)
{
}

void ocl_use_hptr::create_mem_bufs(size_t n)
{
  if(have_bufs)
  {
    free_mem_bufs();
  }
  have_bufs = true;
  qold = new real[n];
  qnew = new real[n];
  cl_int err;
  qold_buf = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(real)*n,
			    qold, &err);
  qnew_buf = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(real)*n,
			    qnew, &err);
}

void ocl_use_hptr::free_mem_bufs()
{
  ocl_base::free_mem_bufs();
  delete[] qold;
  delete[] qnew;
  qold = qnew = nullptr;
}

ocl_use_hptr::~ocl_use_hptr()
{
  free_mem_bufs();
}
