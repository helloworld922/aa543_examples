#include "omp_advect.hpp"
#include <cmath>
#include <algorithm>
#include <iostream>

void omp_advect1::init()
{
  qold = std::vector<double>(2*num_elems);
  // freq: 1 Hz
  for(size_t i = num_elems*.45; i < num_elems*.55; ++i)
  {
    double x = dx * (i-num_elems/2.);
    qold[i] = sin(2.*3.141592653589793*x);
    qold[i+num_elems] = cos(2.*3.141592653589793*x);
  }
  qnew = qold;
}

void omp_advect1::run()
{
  for(size_t t = 0; t < num_steps; ++t)
  {
#pragma omp parallel for
    for(size_t i = 1; i < num_elems-1; ++i)
    {
      qnew[i] = qold[i] - a*dt/dx*(qold[i]-(qold[i+1]+qold[i-1]-qold[i+1+num_elems]+qold[i-1+num_elems])*.5);
      qnew[i+num_elems] = qold[i+num_elems] - a*dt/dx*(qold[i+num_elems]-(qold[i+1+num_elems]+qold[i-1+num_elems]-qold[i+1]+qold[i-1])*.5);
    }
    // periodic BC's
    qnew[0] = qnew[num_elems-2];
    qnew[num_elems-1] = qnew[1];
    qnew[num_elems] = qnew[2*num_elems-2];
    qnew[2*num_elems-1] = qnew[num_elems+1];
    std::swap(qold, qnew);
  }
}


void omp_advect2::init()
{
  qold = std::vector<double>(2*num_elems);
  // freq: 1 Hz
  for(size_t i = num_elems*.45; i < num_elems*.55; ++i)
  {
    double x = dx * (i-num_elems/2.);
    qold[2*i] = sin(2.*3.141592653589793*x);
    qold[2*i+1] = cos(2.*3.141592653589793*x);
  }
  qnew = qold;
}

void omp_advect2::run()
{
  for(size_t t = 0; t < num_steps; ++t)
  {
#pragma omp parallel for
    for(size_t i = 1; i < num_elems-1; ++i)
    {
      qnew[2*i] = qold[2*i] - a*dt/dx*(qold[2*i]-(qold[2*(i+1)]+qold[2*(i-1)]-qold[2*(i+1)+1]+qold[2*(i-1)+1])*.5);
      qnew[2*i+1] = qold[2*i+1] - a*dt/dx*(qold[2*i+1]-(qold[2*(i+1)+1]+qold[2*(i-1)+1]-qold[2*(i+1)]+qold[2*(i-1)])*.5);
    }
    std::swap(qold, qnew);
  }
}
