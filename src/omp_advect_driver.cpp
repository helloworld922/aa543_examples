#include "omp_advect.hpp"
#include "omp_tiled.hpp"
#include "benchmarking.hpp"
#include <iostream>
#include <fstream>
#include <cmath>
#include <thread>

namespace std
{
  std::ostream& operator<<(std::ostream& o, std::vector<std::chrono::nanoseconds>& v)
  {
    o << "[";
    if(v.size())
    {
      o << v[0].count() * 1e-9;
    }
    for(size_t i = 1; i < v.size(); ++i)
    {
      o << ", " << v[i].count() * 1e-9;
    }
    o << "]";
    return o;
  }

  std::ostream& operator<<(std::ostream& o, std::vector<size_t>& v)
  {
    o << "[";
    if(v.size())
    {
      o << v[0];
    }
    for(size_t i = 1; i < v.size(); ++i)
    {
      o << ", " << v[i];
    }
    o << "]";
    return o;
  }

  std::ostream& operator<<(std::ostream& o, std::vector<double>& v)
  {
    o << "[";
    if(v.size())
    {
      o << v[0];
    }
    for(size_t i = 1; i < v.size(); ++i)
    {
      o << ", " << v[i];
    }
    o << "]";
    return o;
  }
}

void test_output(size_t n)
{
#if 1
  {
    std::ofstream out("advect1.out");
    omp_advect1 vm(1, n, 1, 1);
    vm.init();
    for(size_t i = 0; i < 2000; ++i)
    {
      for(size_t j = 0; j < 1; ++j)
      {
	vm.run();
      }
      out << vm.qold << std::endl;
    }
  }
#endif
#if 0
  {
    std::ofstream out("advect2.out");
    omp_advect2 vm(1, n, 1, 1);
    vm.init();
    for(size_t i = 0; i < 100; ++i)
    {
      vm.run();
      out << vm.qold << std::endl;
    }
  }
#endif
#if 0
  {
    std::ofstream out("advect3.out");
    omp_tiled vm(1, n, 1, 256,1);
    vm.init();
    for(size_t i = 0; i < 50; ++i)
    {
      out << vm.qold << std::endl;
      vm.run();
    }
  }
#endif
}

void do_benchmarking()
{
  using namespace std::chrono;
  constexpr size_t min_iters = 2;
  constexpr size_t max_iters = 10000;
  constexpr size_t num_steps = 1000;
  size_t n_min = 1310720*4;
  size_t n_max = 10000000;
  std::vector<size_t> ns;
  milliseconds stop_time(250);
  for(size_t num_t = std::thread::hardware_concurrency(); num_t > 0 ; --num_t)
  {
    std::vector<nanoseconds> vm1_times;
    std::vector<nanoseconds> vm2_times;
    std::vector<nanoseconds> vm3_times;

    for(size_t n = n_min; n < n_max; n = 2*n)
    {
      //std::cout << num_t << ", " << n << ": ";
      if(num_t == 1)
      {
	ns.push_back(n);
      }
#if 0
      {
	omp_advect1 vm(num_t, n, 1, num_steps);
	vm1_times.push_back(benchmark(min_iters, max_iters, stop_time, vm));
      }
#endif
      //std::cout << ", ";
#if 0
      {
	omp_advect2 vm(num_t, n, 1, num_steps);
	vm2_times.push_back(benchmark(min_iters, max_iters, stop_time, vm));
      }
#endif
      {
	omp_tiled vm(num_t, n, 1, 16384, num_steps/2);
	vm3_times.push_back(benchmark(min_iters, max_iters, stop_time, vm));
      }
      //std::cout << std::endl;
    }
#if 0
    std::cout << "vm1.append(" << vm1_times << ")" << std::endl;
    std::cout << "vm2.append(" << vm2_times << ")" << std::endl;
#endif
    std::cout << "vm3.append(" << vm3_times << ")" << std::endl;
  }
  std::cout << "ns = " << ns << std::endl;
}

int main()
{
  //test_output(400);
  do_benchmarking();
  return 0;
}
