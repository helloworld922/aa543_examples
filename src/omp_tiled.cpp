#include "omp_tiled.hpp"
#include <cmath>
#include <algorithm>
#include <iostream>

omp_tiled::omp_tiled(size_t num_threads,
		     size_t num_elems,
		     double a,
		     size_t ts,
		     size_t num_steps) :
  omp_advect_base(num_threads, num_elems, a, num_steps),
  num_tiles((num_elems % ts == 0) ? (num_elems / ts) : (num_elems / ts + 1)),
  tile_size(ts)
{
}

void omp_tiled::init()
{
  qold = std::vector<double>(2*num_elems);
  for(size_t i = num_elems*.45; i < num_elems*.55; ++i)
  {
    double x = dx * (i-num_elems/2.);
    qold[i] = sin(2.*3.141592653589793*x);
    qold[i+num_elems] = cos(2.*3.141592653589793*x);
  }
  qnew = qold;
}

void omp_tiled::run()
{
  const double cfl = a*dt/dx;
  for(size_t t = 0; t < num_steps; ++t)
  {
    // main tile advance
#pragma omp parallel for
    for(size_t i = 0; i < num_tiles; ++i)
    {
      // first step
      size_t idx = i*tile_size+1;
      for(size_t j = 1; j <= tile_size && idx < num_elems-1; ++j,++idx)
      {
	const double un = qold[idx-1];
	const double u = qold[idx];
	const double up = qold[idx+1];
	const double vn = qold[idx+num_elems-1];
	const double v = qold[idx+num_elems];
	const double vp = qold[idx+num_elems+1];
	qnew[idx] = u-cfl*(u-(up+un-vp+vn)*.5);
	qnew[idx+num_elems] = v-cfl*(v-(vp+vn-up+un)*.5);
      }
#if 1
      // second step
      size_t loff = i ? 1 : 0;
      size_t roff = (i == num_tiles-1) ? 0 : 1;
      idx = i*tile_size+1+loff;
      for(size_t j = loff+1; j <= tile_size-roff && idx < num_elems-1; ++j,++idx)
      {
	const double un = qnew[idx-1];
	const double u = qnew[idx];
	const double up = qnew[idx+1];
	const double vn = qnew[idx+num_elems-1];
	const double v = qnew[idx+num_elems];
	const double vp = qnew[idx+num_elems+1];
	qold[idx] = u-cfl*(u-(up+un-vp+vn)*.5);
	qold[idx+num_elems] = v-cfl*(v-(vp+vn-up+un)*.5);
      }
#endif
    }
#if 0
    std::swap(qnew, qold);
#endif
    // tile boundaries
    //#pragma omp parallel for
    for(size_t i = 1; i < num_tiles; ++i)
    {
      {
	const size_t idx = i*tile_size;
	// right boundary
	const double un = qnew[idx-1];
	const double u = qnew[idx];
	const double up = qnew[idx+1];
	const double vn = qnew[idx+num_elems-1];
	const double v = qnew[idx+num_elems];
	const double vp = qnew[idx+num_elems+1];
	qold[idx] = u-cfl*(u-(up+un-vp+vn)*.5);
	qold[idx+num_elems] = v-cfl*(v-(vp+vn-up+un)*.5);
      }
      {
	const size_t idx = i*tile_size+1;
	// left boundary
	const double un = qnew[idx-1];
	const double u = qnew[idx];
	const double up = qnew[idx+1];
	const double vn = qnew[idx+num_elems-1];
	const double v = qnew[idx+num_elems];
	const double vp = qnew[idx+num_elems+1];
	qold[idx] = u-cfl*(u-(up+un-vp+vn)*.5);
	qold[idx+num_elems] = v-cfl*(v-(vp+vn-up+un)*.5);
      }
    }
  }
}
