#include "vm_mult.hpp"
#include <algorithm>
#include <random>

std::vector<double> rand_vec(size_t n)
{
  std::random_device rd;
  std::default_random_engine gen(rd());
  std::uniform_real_distribution<> dis(0,1);
  std::vector<double> mat(n);
  for(auto& i : mat)
  {
    i = dis(gen);
  }
  return mat;
}

void vm_mult_base::init()
{
  mat = rand_vec(n*n);
  vec = rand_vec(n);
  res = std::vector<double>(n);
}

void vm_mult1::run()
{
  for(size_t row = 0; row < n; ++row)
  {
    res[row] = 0;
    for(size_t col = 0; col < n; ++col)
    {
      res[row] += mat[row*n+col] * vec[col];
    }
  }
}

void vm_mult2::run()
{
  std::fill(res.begin(), res.end(), 0.);
  for(size_t col = 0; col < n; ++col)
  {
    for(size_t row = 0; row < n; ++row)
    {
      res[row] += mat[row*n+col] * vec[col];
    }
  }
}

