#include "vm_mult.hpp"
#include "benchmarking.hpp"
#include <iostream>
#include <cmath>

namespace std
{  
  std::ostream& operator<<(std::ostream& o, std::vector<std::chrono::nanoseconds>& v)
  {
    o << "[";
    if(v.size())
    {
      o << v[0].count() * 1e-9;
    }
    for(size_t i = 1; i < v.size(); ++i)
    {
      o << ", " << v[i].count() * 1e-9;
    }
    o << "]";
    return o;
  }

  std::ostream& operator<<(std::ostream& o, std::vector<size_t>& v)
  {
    o << "[";
    if(v.size())
    {
      o << v[0];
    }
    for(size_t i = 1; i < v.size(); ++i)
    {
      o << ", " << v[i];
    }
    o << "]";
    return o;
  }
}

int main()
{
  using namespace std::chrono;
  constexpr size_t min_iters = 4;
  constexpr size_t max_iters = 10000;
  size_t n_min = 1;
  size_t n_max = 1<<15;
  std::vector<size_t> ns;
  std::vector<nanoseconds> vm1_times;
  std::vector<nanoseconds> vm2_times;
  milliseconds stop_time(1000);

  for(size_t n = n_min; n < n_max; n = ceil(n*1.5))
  {
    std::cout << n << ": ";
    ns.push_back(n);
    {
      vm_mult1 vm(n);
      vm1_times.push_back(benchmark(min_iters, max_iters, stop_time, vm));
    }
    std::cout << ", ";
    {
      vm_mult2 vm(n);
      vm2_times.push_back(benchmark(min_iters, max_iters, stop_time, vm));
    }
    std::cout << std::endl;
  }
  std::cout << "ns = " << ns << std::endl;
  std::cout << "vm1 = " << vm1_times << std::endl;
  std::cout << "vm2 = " << vm2_times << std::endl;
  return 0;
}
